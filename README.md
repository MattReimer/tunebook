# Matt's Tunebook

All of this is in ABC notation

## Getting started with ABC notation

There are some great documents out there:

* [The abc music standard document](http://abcnotation.com/wiki/abc:standard:v2.1)
* [The Quick Refence Cheat Sheet](http://www.stephenmerrony.co.uk/uploads/ABCquickRefv0_6.pdf)
* [Chords](http://trillian.mit.edu/~jc/music/abc/doc/ABCtut_Chords.html)
* [Some great software](http://abcnotation.com/software)
* [Paul Hardy's Tunebook Process](http://www.pghardy.net/concertina/tunebooks/tunebook_process.html)

I like [EasyABC](http://www.nilsliberg.se/ksp/easyabc/). There are Windows and OSX versions available. It can also import MIDI files which is awesome.

On iOS or Android try [Tunebook](https://itunes.apple.com/ca/app/tunebook-for-ipad/id368413919?mt=8) for collecting all your tunes.

## Where to get ABC music and tunes

Tune archives are an exciting exercise in frustration and agony. They all look like they were vommited out of an angry badger in 1995 and then left in a warm, moist place for the next 15 years. The exception that proves the rule is thesession.org.

* [TheSession.org](http://thesession.org/) This is THE source for irish or scottish music.
* [Trad Tune Archive](http://www.tunearch.org/wiki/TTA) (work in progress)
* [Ceolas](http://www.ceolas.org/tunes/)
* [The Fiddler's Companion](http://www.ibiblio.org/fiddlers/)
* [New Hampshire Old-time country Dance Web site](http://nhcountrydance.com/music/home.html)
* [Tune collections on the internet](http://www.biteyourownelbow.com/webtunes.htm)


## Some guidelines:

* Don't commit midi files please. They just take up space and it's kind of an anti-pattern.
* I'm not sure about how to organize tunes yet. One per file? One file per set list?